///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  Paulo Baldovi <pbaldovi@hawaii.edu>
/// @date    18 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
using namespace std;

int main( int argc, char* argv[] ) {
   string inputName = argv[1]; 
   //std::cout << "Crazy Cat Lady!" << std::endl ;
   
   cout << "Oooooh! " << inputName << " you’re so cute!" << endl;

   return 0;
}
